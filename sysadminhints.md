# Common tasks

## Follow the log
`sudo -u www-data /var/nextcloud/nextcloud/occ log:watch`

## View parts of log messages
`tail /var/nextcloud/data/nextcloud.log | jq -r '"\(.time) \(.app): \(.url) \(.message)"'`

## Check for updates
`(cd /var/nextcloud/nextcloud && sudo -u www-data php occ update:check)`

## Upgrade NextCloud
`(cd /var/nextcloud/nextcloud/updater && sudo -u www-data php updater.phar)`

## Upgrade all apps
`(cd /var/nextcloud/nextcloud && sudo -u www-data php occ app:update --all)`

# Tidbits of knowledge

## Transactional file locking

Without `memcache.locking` set, Nextcloud will be using the ordinary
SQL database for transactional file locking and warn about that on the
Security & setup warnings page. Installing Redis and using it for that
is an option when this becomes a problem.

https://docs.nextcloud.com/server/30/admin_manual/configuration_files/files_locking_transactional.html


## PostgreSQL

A.k.a postgres, postgresql and pgsql.

A PostgreSQL server cluster is a collection of databases served by a postgres(1) instance.

One can run multiple versions of postgres simultaneously:
```
root@nextcloud-01:~# pg_lsclusters
Ver Cluster Port Status Owner    Data directory              Log file
14  main    5432 online postgres /var/lib/postgresql/14/main /var/log/postgresql/postgresql-14-main.log
15  main    5433 online postgres /var/lib/postgresql/15/main /var/log/postgresql/postgresql-15-main.log
16  main    5434 online postgres /var/lib/postgresql/16/main /var/log/postgresql/postgresql-16-main.log
```

Database migration from a pgsql perspective:
[19.6. Upgrading a PostgreSQL Cluster](https://www.postgresql.org/docs/current/upgrading.html)
says `pg_dumpall` but also links to [pg_upgrade](https://www.postgresql.org/docs/current/pgupgrade.html).

We're running postgresql-{14,15,16} from postgresql.org, unclear
exactly why but presumably because bullseye didn't have a modern
enough pgsql at the time of installation (2022-08-23).

```
root@nextcloud-01:~# cat /etc/apt/sources.list.d/pgdg.list
deb https://apt.postgresql.org/pub/repos/apt bullseye-pgdg main
```

### Upgrading

From /usr/share/doc/postgresql-common/README.Debian.gz:
```
Default clusters and upgrading
------------------------------
When installing a postgresql-NN package from scratch, a default
cluster 'main' will automatically be created. This operation is
equivalent to doing 'pg_createcluster NN main --start'.

Due to this default cluster, an immediate attempt to upgrade an
earlier 'main' cluster to a new version will fail and you need to
remove the newer default cluster first. E. g., if you have
postgresql-9.6 installed and want to upgrade to 11, you first install
postgresql-11:

  apt-get install postgresql-11

Then drop the default 11 cluster that was just created:

  pg_dropcluster 11 main --stop

And then upgrade the 9.6 cluster to the latest installed version (e. g. 11):

  pg_upgradecluster 9.6 main
```
