# nextcloud

This repository collects user instructions and tips & tricks, and is used for tracking operational issues with [our Nextcloud instance](https://nextcloud.glasklarteknik.se/).
