This document lists a few hints for users of Glasklar Nextcloud instance at https://nextcloud.glasklarteknik.se/

## Configuring phone app Nextcloud for file sharing

### On your laptop
1. Log in to https://nextcloud.glasklarteknik.se/ using a web browser
2. Click your avatar, rightmost in the menu bar
3. Click Settings in the drop down menu
4. Click Security in the left panel
5. Scroll down to Devices & sessions
6. Enter an "app name" in the empty input field and click the "Create new app password" button
7. Click "Show QR code"

### On your phone
1. In Nextcloud by Nextcloud
   - https://f-droid.org/en/packages/com.nextcloud.client/
   - https://github.com/nextcloud/android
2. Add new account by clicking on your avatar in the upper right corner
3. Select add account
4. Select using QR code
5. Scan the QR code in the web browser

NOTE: Ignore step 1 if your app is newly installed and doesn't have any other accounts.


## Configuring calendar and task lists for Android phones

### Configuring phone app DAVx5 for calendar and tasks

First, on your laptop, you create a new "app password" just like you
did for the Nextcloud app above.

Then, on your phone:
1. Install DAVx5 by bitfire web engineering
   - https://f-droid.org/packages/at.bitfire.davdroid/
   - https://github.com/bitfireAT/davx5-ose
2. Press the plus sign in the lower right corner
3. Enter your `@glasklarteknik.se` email address
4. Type in the app password from the web browser session on your laptop
5. Press login and create new account
6. Select contacts under CARDDAV and select calendars under CALDAV and
   push the refresh button in the lower right corner

### Configuring Etar for calendars

1. Install Etar - OpenSource Calendar by Suhail
   - https://f-droid.org/packages/ws.xsoh.etar/
   - https://github.com/Etar-Group/Etar-Calendar
2. TODO

### Configuring OpenTasks

1. Install OpenTasks from dmfs GmbH
   - https://github.com/dmfs/opentasks
   - https://f-droid.org/en/packages/org.dmfs.tasks/
2. TODO

## Configuring calendar for iOS 17.5.1 (iPhone 14)

`Settings -> Calendar -> Accounts -> Other -> AddCalDAV account`, enter:

    Server: nextcloud.glasklarteknik.se
    Username: rgdd@glasklarteknik.se (use your own email)
    Password: APP_PASSWORD_GENERATED_IN_NC
    Description: ARBITRARY

`APP_PASSWORD_GENERATEDIN_NC` is generated in nextcloud under `Settings ->
Security`. 

## Automatic expiration of file shared via link

Files can be shared via a link by clicking the sharing symbol and selecting "Share link".

Such links have a default expire date of one week from the date of creation.

Each morning at 09:00 you will receive a notification for each of your links that are about to expire withing the next 24 hours.
Historical notifications can be viewed by clicking the bell symbol in the Nextcloud menu, next to the search button.

Read more about file sharing: https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/file_sharing_configuration.html#creating-persistent-file-shares


## Files and synchronization

Install [the client](https://nextcloud.com/install/#install-clients)
for your device. If you're using macOS, use the `macOS Virtual files
12+ (64 bit, universal)` desktop client.

> TODO: add configuration instructions

### DavFS mounts (Linux)

Instead of synchronizing files between user device and server you can
use a simple "drive mount". Access to files on server requires a
network connection and a healthy server.

- Assuming SERVER=dns-name-for-nextcloud-server, MOUNTPOINT=local-path, USERNAME=nextcloud-username, do
```
sudo apt install davfs2
sudo echo "https://$SERVER/remote.php/dav/files/$USER/ $MOUNTPOINT davfs user,rw,noauto 0 0" >> /etc/fstab
mkdir -m 0700 ~/.davfs2
    
: create an access token at https://$SERVER/index.php/settings/user/security -> Create new app password
echo "https://$SERVER/remote.php/dav/files/$USERNAME/ $USERNAME $(cat token)" >> ~/.davfs2/secrets

mkdir $MOUNTPOINT
mount $MOUNTPOINT
```

See [Accessing Nextcloud files using WebDAV -- WebDAV configuration](https://docs.nextcloud.com/server/26/user_manual/en/files/access_webdav.html#webdav-configuration)
for more info.

- https://docs.nextcloud.com/server/20/user_manual/en/files/access_webdav.html

## TOTP with yubioath-desktop (alternative to authenticator app on a phone)

Start by generating and saving the backup codes in the nextcloud UI.  Then:

    # apt install yubioath-desktop
	$ yubioath-desktop

Click the three dots, `Add account`.  Enter:

    Issuer: ARBITRARY
	Account name: rgdd@glasklarteknik.se@nextcloud.glasklarteknik.se (use your own email)
	Secret key: NEXTCLOUD_SECRET

`NEXTCLOUD_SECRET` is provided by nextcloud when enabling TOTP.  Once the
account is added, click on it in the `yubioath-desktop` to get the code.  Repeat
this (click the account) whenever a new TOTP code is needed for login somewhere.
